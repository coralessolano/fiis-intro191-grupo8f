# UNI DRIVERS
# INTEGRANTES :
 CORALES SOLANO ELOHIM(LIDER DEL GRUPO)

 BARDALES ORBEGOSO GIANCARLO
 
 CARHUAS BASTIDAS JESUS
 
 CAYCHO ALEJOS JAIR
 
# VIDEO DE MUESTRA :
https://www.youtube.com/watch?v=IAeJLOHcm9A&feature=youtu.be&fbclid=IwAR3Zt1DWr_udU1H6uJrTq1Hszh4YuGxvlaGjawK2han5Us40m2qWVe8IxQg
# VIDEO DE MUESTRA(final) :
https://www.youtube.com/watch?v=_WiTaoCMoik&feature=youtu.be

# TEMA DE TRABAJO FINAL
 Uno de los problema de los estudiantes de la Universidad Nacional de Ingeniería es la forma de transportarse de manera incómoda y esperar a ciertos horarios
 escasos, para ello hemos desarrollado una aplicación que consiste en facilitar al usuario el método de transporte convencional, este suele dirigirse a algún paradero y 
 esperar sin tener alguna certeza del tiempo a demorar
 de partida con el automóvil necesitado.
 La aplicación ofrece al usuario: mostrar qué autos están disponibles junto con la información del chofer y la ruta que tomará,
 una características similar a las aplicaciones de taxis en linea,
 claro está que cada auto cuenta con distintas rutas 
 y paraderos específicos en el horario que el usuario podrá escoger dentro de una lista que se le brindará junto con el precio 
 correspondiente a la ruta, todos los autos parten de la universidad y tendrán un 
 horario específico de salida, los métodos de pago se pueden realizar en efectivo o con alguna tarjeta de crédito o dédito, 
 la idea es brindar a los estudiantes un método de transporte más confiable, cómodo y barato ya que el servicio de transporte 
 que brinda la universidad es muy limitado y algunas veces incómodo, esta aplicación permite un viaje seguro y barato además de mayores lugares para partir y arribar.

