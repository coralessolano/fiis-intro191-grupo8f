#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

struct conductor{
	string nombre;
	int edad;
	string placa;
	string referencia;
	string codigo;
}
conductorS_M1_V1={"MAURICIO",20, "SD 3478", "ENSENIA KARATE LOS JUEVES","S0M10V1"},
conductorS_M2_V1={"MING" ,34, "EE 4533","ENSENIA IDIOMAS COMO INGLES Y FRANCES LOS DOMINGOS","S0M20V1"},
conductorS_M3_V1={"JULIANA",60,"DC 4326", "ES MUY AMABLE Y CANTA BIEN","S0M30V1"},
conductorS_T1_V1={"KATY",49, "GH 4478", "FUE CAMPEONA DE BOXEO EN 1999","S0N10V1"},
conductorS_T2_V1={"MARIA",34,"TN 3566", "TIENE UN ACENTO FRANCES, ES DE PADRES CANADIENSES","S0N20V1"},
conductorS_T3_V1={"FLOR",30,"CC 3421","ES JARDINERA, CHOFER Y MUY LOCUAZ","S0N30V1"},
conductorS_N1_V1={"NICOLE",23,"TM 3443", "ANIOS, ESTUDIA TURISMO Y CONOCE LA MITAD DE PERU-","S0T10V1"},
conductorS_N2_V1={"HANNAH",28,"GA 8945","ESTUDIO' INGENIERI'A CIVIL EN LA UNI","S0T20V1"},
conductorS_N3_V1={"CARL",33,"NG 4769", "ESTUDIO' FI'SICA EN LA UNI, LE GUSTA MANEJAR","S0T30V1"};

void submenuvehiculoSUR(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuparaderoSUR(string puntero2);
void submenuboleto(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7, string puntero8);

void submenuvehiculoSUR_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoSUR_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);

void submenuvehiculoSUR(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){

	string punteroM1;

	punteroM1="HORARIO M1";
	if(puntero3==punteroM1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroM2;

	punteroM2="HORARIO M2";
	if(puntero3==punteroM2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroM3;

	punteroM3="HORARIO M3";
	if(puntero3==punteroM3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroT1;

	punteroT1="HORARIO T1";
	if(puntero3==punteroT1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}
	string punteroT2;

	punteroT2="HORARIO T2";
	if(puntero3==punteroT2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}
	string punteroT3;

	punteroT3="HORARIO T3";
	if(puntero3==punteroT3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		if(g==1){submenuvehiculoSUR_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroN1;

	punteroN1="HORARIO N1";
	if(puntero3==punteroN1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHICULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}
	string punteroN2;

	punteroN2="HORARIO N2";
	if(puntero3==punteroN2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}
	string punteroN3;

	punteroN3="HORARIO N3";
	if(puntero3==punteroN3
			){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoSUR_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoSUR(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}
}

void submenuvehiculoSUR_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_M1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_M1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_M1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_M1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_M1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
	cin >> h;
			system("cls");

			if(h==1){submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_M2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_M2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_M2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_M2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_M2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_M3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_M3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_M3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_M3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_M3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_T1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_T1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_T1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_T1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_T1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_T2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_T2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_T2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_T2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_T2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_T3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_T3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_T3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_T3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_T3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_N1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_N1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_N1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_N1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_N1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_N2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_N2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_N2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_N2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_N2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoSUR_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorS_N3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorS_N3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorS_N3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorS_N3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorS_N3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoSUR(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoSUR_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
