#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

struct conductor{
	string nombre;
	int edad;
	string placa;
	string referencia;
	string codigo;
}
conductorO_M1_V1={"VALERIE",25 , "ER 3425" , "PUEDE IMITAR CON LOS SILBIDOS MUCHOS SONIDOS","O0M10V1" },
conductorO_M2_V1={"MANIE",35, "LP 3452", "TAMBIE'N ES ALBAN-IL DESDE LOS 30","O0M20V1" },
conductorO_M3_V1={"BENNIE",45, "CP 5907", "COLECCIONA JETS DE JUGUETE","O0M30V1" },
conductorO_T1_V1={"VLADIMIR",34, "TZ 3408" ,  "TIENE UN CLUB DE JAZZ","O0T10V1" },
conductorO_T2_V1={"VADIM",22, "DF 9003" , "TIENE UNA OREJA MAS GRANDE QUE LA OTRA","O0T20V1" },
conductorO_T3_V1={"CHRISTINE",50, "RD 2234", "APRENDIO A MANEJAR DESDE LOS 20","O0T30V1" },
conductorO_N1_V1={"JUSTINE",41, "BH 3353", "EN LOS FINES DE SEMANA CORTA CABELLO","O0N10V1" },
conductorO_N2_V1={"TONY",55, "TO 3974", "ES MUY INTELIGENTE Y LE APODAN EL AMOROSO","O0N20V1" },
conductorO_N3_V1={"ELOHIM",20,"CS 0412","ES ESTUDIANTE DE INGENIERIA DE SISTEMAS DE LA UNI Y NO ES MUY BUEN PROGRAMADOR","O0N30V1"};

void submenuvehiculoOESTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuparaderoOESTE(string puntero3);
void submenuboleto(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7, string puntero8);

void submenuvehiculoOESTE_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoOESTE_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);

void submenuvehiculoOESTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){

	string punteroM1;

	punteroM1="HORARIO M1";
	if(puntero3==punteroM1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroM2;

	punteroM2="HORARIO M2";
	if(puntero3==punteroM2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroM3;

	punteroM3="HORARIO M3";
	if(puntero3==punteroM3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}



	string punteroT1;

	punteroT1="HORARIO T1";
	if(puntero3==punteroT1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroT2;

	punteroT2="HORARIO T2";
	if(puntero3==punteroT2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroT3;

	punteroT3="HORARIO T3";
	if(puntero3==punteroT3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}



	string punteroN1;

	punteroN1="HORARIO N1";
	if(puntero3==punteroN1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroN2;

	punteroN2="HORARIO N2";
	if(puntero3==punteroN2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroN3;

	punteroN3="HORARIO N3";
	if(puntero3==punteroN3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoOESTE_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoOESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}



}


void submenuvehiculoOESTE_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_M1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_M1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_M1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_M1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_M1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_M2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_M2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_M2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_M2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_M2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_M3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_M3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_M3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_M3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_M3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_T1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_T1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_T1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_T1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_T1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_T2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_T2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_T2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_T2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_T2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_T3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_T3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_T3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_T3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_T3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_N1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_N1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_N1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_N1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_N1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_N2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_N2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_N2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_N2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_N2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoOESTE_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorO_N3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorO_N3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorO_N3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorO_N3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorO_N3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoOESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoOESTE_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
