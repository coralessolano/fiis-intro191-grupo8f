#include <iostream>
#include <string>
#include <stdlib.h>
void submenuturno();

using namespace std;

void submenuparaderoOESTE(string puntero2);
void submenuparaderoESTE(string puntero2);
void submenuparaderoSUR(string puntero2);
void submenuparaderoNORTE(string puntero2);
void submenuruta(string puntero1);


void submenuvehiculoOESTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);

void submenuvehiculoESTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);

void submenuvehiculoSUR(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);

void submenuvehiculoNORTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);



	void submenuparaderoNORTE(string puntero2){
		int f;
		string ruta;
		string precio;
		string distancia;
		string tiempo;
	    cout << "\n   ****************************************\n";
		cout << "\n            PARADEROS RUTA NORTE  \n";
		cout << "\n         1) PARADERO-LA MERCED \n";
		cout << "\n         2) PARADERO-POLITE-CNICO \n";
	    cout << "\n         3) PARADERO-CORRREO \n";
	    cout << "\n         4) PARADERO-ESPANIA \n";
	    cout << "\n         5) PARADERO-PUNO \n";
	    cout << "\n         6) PARADERO-SANTA ROSA \n";
	    cout << "\n         7) PARADERO-BELAUNDE \n";
	    cout << "\n         8) PARADERO-PASCANA \n";
	    cout << "\n         9) PARADERO-VELASCO \n";
	    cout << "\n         10) PARADERO-ENTRADA A COLLIQUE \n";
	    cout << "\n         11) PARADERO-SAN FELIPE \n";
	    cout << "\n         12) PARADERO-MAMA OCLLO \n";
	    cout << "\n         13) PARADERO-CIRO ALEGRI'A \n";
	    cout << "\n         14) PARADERO-MANUEL PRADO \n";
	    cout << "\n         15) ATRA'S \n";
	    cout << "\n   ****************************************\n";
	    cout << "\n";
		cin >> f;
		system("cls");

		if(f>0 && f<15) cout << "\n   !DESTINO ELEGIDO CON E'XITO!\n";

		if(f==1){

			ruta="NORTE - LA MERCED";

			precio="11";

			distancia="6,1 KM";

			tiempo="19";
		}
		if(f==2){

			ruta="NORTE - POLITE-CNICO";

			precio="13";

			distancia="7 KM";

			tiempo="21";
		}
		if(f==3){

			ruta="NORTE - CORRREO";

			precio="14";

			distancia="7,6 KM";

			tiempo="23";
		}
		if(f==4){

			ruta="NORTE - ESPANIA";

			precio="15";

			distancia="8,1 KM";

			tiempo="25";
			}
		if(f==5){

			ruta="NORTE - PUNO";

			precio="16";

			distancia="8,5 KM";

			tiempo="27";
			}
		if(f==6){

			ruta="NORTE - SANTA ROSA";

			precio="17";

			distancia="9 KM";

			tiempo="29";
			}
		if(f==7){

			ruta="NORTE - BELAUNDE";

			precio="18";

			distancia="9,9 KM";

			tiempo="31";
			}
		if(f==8){

			ruta="NORTE - PASCANA";

			precio="19";

			distancia="10,8 KM";

			tiempo="34";
			}
		if(f==9){

			ruta="NORTE - VELASCO";

			precio="22";

			distancia="12,4 KM";

			tiempo="40";
			}
		if(f==10){

			ruta="NORTE - ENTRADA A COLLIQUE";

			precio="24";

			distancia="12,9 KM";

			tiempo="42";
			}
		if(f==11){

			ruta="NORTE - SAN FELIPE";

			precio="26";

			distancia="14,1 KM";

			tiempo="45";
			}
		if(f==12){

			ruta="NORTE - MAMA OCLLO";

			precio="29";

			distancia="16,3 KM";

			tiempo="50";
			}
		if(f==13){

			ruta="NORTE - CIRO ALEGRI'A";

			precio="32";

			distancia="17,6 KM";

			tiempo="55";
			}
		if(f==14){

			ruta="NORTE - MANUEL PRADO";

			precio="34";

			distancia="18,7 KM";

			tiempo="61";
			}
		if(f<15 && f>0){
		submenuvehiculoNORTE(puntero2,ruta,precio,distancia,tiempo);}

		if(f==15){
			submenuruta(puntero2);
		}
		else{
			cout << "\n!ERROR!- HA SELECCIONADO UNA OPCIO-N INVALIDA\n";
			submenuparaderoNORTE(puntero2);
		}
	}


	void submenuparaderoSUR(string puntero2){
		int f;
		string ruta;
		string precio;
		string distancia;
		string tiempo;
	    cout << "\n   ****************************************\n";
		cout << "\n              PARADEROS RUTA SUR            \n";
	    cout << "\n      1. PARADERO-PUENTE TRUJILLO \n";
	    cout << "\n      2. PARADERO-PUENTE NUEVO \n";
	    cout << "\n      3. PARADERO-SANTA ANITA \n";
	    cout << "\n      4. PARADERO-QUECHUAS \n";
	    cout << "\n      5. PARADERO-JAVIER PRADO \n";
	    cout << "\n      6. PARADERO-PRIMAVERA \n";
	    cout << "\n      7. PARADERO-BENAVIDES \n";
	    cout << "\n      8. PARADERO-ATOCONGO \n";
	    cout << "\n      9. PARADERO-CIUDAD \n";
	    cout << "\n      10. PARADERO-HOSPITAL MAR'A AUXILIADORA \n";
	    cout << "\n      11. PARADERO-SAN GABRIEL \n";
	    cout << "\n      12. ATRA'S \n";
	    cout << "\n   ****************************************\n";
	    cout << "\n";
	    cin >> f;
		system("cls");

		if(f>0 && f<12) cout << "\n   ¡DESTINO ELEGIDO CON E'XITO!\n";
		if(f==1){

			ruta="SUR - PUENTE TRUJILLO";

			precio="7";

			distancia="4,9 KM";

			tiempo="15 ";
		}
		if(f==2){

			ruta="SUR - PUENTE NUEVO";

			precio="12";

			distancia="9 KM";

			tiempo="23";
		}
		if(f==3){

			ruta="SUR - SANTA ANITA";

			precio="17";

			distancia="13,3 KM";

			tiempo="26";
		}
		if(f==4){

			ruta="SUR - QUECHUAS";

			precio="21";

			distancia="15,8";

			tiempo="36";
		}
		if(f==5){

			ruta="SUR - JAVIER PRADO";

			precio="22";

			distancia="16,6 KM";

			tiempo="";
		}
		if(f==6){

			ruta="SUR - PRIMAVERA";

			precio="26";

			distancia="19,6 KM";

			tiempo="54";
		}
		if(f==7){

			ruta="SUR - BENAVIDES";

			precio="29";

			distancia="22,2 KM";

			tiempo="56";
		}
		if(f==8){

			ruta="SUR - ATOCONGO";

			precio="32";

			distancia="24 KM";

			tiempo="59";
		}
		if(f==9){

			ruta="SUR - CIUDAD";

			precio="37";

			distancia="28 KM";

			tiempo="70";
		}
		if(f==10){

			ruta="SUR - HOSPITAL MARI'A AUXILIADORA";

			precio="40";

			distancia="29,5 KM";

			tiempo="79";
		}
		if(f==11){

			ruta="SUR - SAN GABRIEL";

			precio="42";

			distancia="30 KM";

			tiempo="81";
		}

		if(f<12 && f>0){
				submenuvehiculoSUR(puntero2,ruta,precio,distancia,tiempo);}

		if(f==12){
			submenuruta(puntero2);
		}
		else{
			cout << "\n!ERROR!- HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuparaderoSUR(puntero2);
		}
	}


	void submenuparaderoESTE(string puntero2){
		int f;
		string ruta;
		string precio;
		string distancia;
		string tiempo;
	    cout << "\n   ****************************************\n";
		cout << "\n              PARADEROS RUTA ESTE            \n";
	    cout << "\n        1. PARADERO-CAJA DE AGUA \n";
	    cout << "\n        2. PARADERO-PIRA'MIDE DEL SOL \n";
	    cout << "\n        3. PARADERO-METRO HACIENDA \n";
	    cout << "\n        4. PARADERO-LOS POSTES \n";
	    cout << "\n        5. PARADERO-22 SAN CARLOS \n";
	    cout << "\n        6. PARADERO-CANTO REY \n";
	    cout << "\n        7. PARADERO-METRO 8 \n";
	    cout << "\n        8. PARADERO-SAN MARTI'N \n";
	    cout << "\n        9. PARADERO-SANTA ROSA \n";
	    cout << "\n        10. PARADERO-BAYOBAR \n";
	    cout << "\n        11. PARADERO-PARADERO 5 \n";
	    cout << "\n        12. ATRA'S \n";
	    cout << "\n   ****************************************\n";
	    cout << "\n";
	    cin >> f;
		system("cls");

		if(f>0 && f<12) cout << "\n   ¡DESTINO ELEGIDO CON E'XITO!\n";

		if(f==1){

			ruta="ESTE - CAJA DE AGUA";

			precio="13";

			distancia="8,5 KM";

			tiempo="25";
		}
		if(f==2){

			ruta="ESTE - PIRA'MIDE DEL SOL";

			precio="16";

			distancia="10,3 KM";

			tiempo="30";
		}
		if(f==3){

			ruta="ESTE - METRO HACIENDA";

			precio="18";

			distancia="11,7";

			tiempo="36";
		}
		if(f==4){

			ruta="ESTE - LOS POSTES";

			precio="19";

			distancia="12,6 KM";

			tiempo="42";
		}
		if(f==5){

			ruta="ESTE - 22 SAN CARLOS";

			precio="22";

			distancia="14 KM";

			tiempo="45";
		}
		if(f==6){

			ruta="ESTE - CANTO REY";

			precio="23";

			distancia="14,9 KM";

			tiempo="49";
		}
		if(f==7){

			ruta="ESTE - METRO 8";

			precio="24";

			distancia="15,2";

			tiempo="53";
		}
		if(f==8){

			ruta="ESTE - SAN MARTI'N";

			precio="25";

			distancia="15,5 KM";

			tiempo="54";
		}
		if(f==9){

			ruta="ESTE - SANTA ROSA ";

			precio="26";

			distancia="16,5";

			tiempo="55";
		}
		if(f==10){

			ruta="ESTE - BAYOBAR";

			precio="28";

			distancia="17,7";

			tiempo="62";
		}
		if(f==11){

			ruta="ESTE - PARADERO 5";

			precio="30";

			distancia="19,2 KM";

			tiempo="68";
		}

		if(f<12 && f>0){
		submenuvehiculoESTE(puntero2,ruta,precio,distancia,tiempo);}

		if(f==12){
			submenuruta(puntero2);
		}
		else{
			cout << "\n!ERROR!- HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuparaderoESTE(puntero2);
		}
	}


	void submenuparaderoOESTE(string puntero2){
		int f;
		string ruta;
		string precio;
		string distancia;
		string tiempo;
	    cout << "\n   ****************************************\n";
		cout << "\n             PARADEROS RUTA OESTE           \n";
	    cout << "\n       1. PARADERO - CAQUETA\n";
	    cout << "\n       2. PARADERO - RAMO'N CASTILLA\n";
	    cout << "\n       3. PARADERO - AV. TINGO MARI'A\n";
	    cout << "\n       4. PARADERO - MERCADO COLONIAL\n";
	    cout << "\n       5. PARADERO - MEGAPLAZA BELLAVISTA\n";
	    cout << "\n       6. PARADERO - AV. SANTA ROSA \n";
	    cout << "\n       7. ATRA'S \n";
	    cout << "\n   ****************************************\n";
	    cout << "\n";
	    cin >> f;
		system("cls");

		if(f>0 && f<7) cout << "\n   !DESTINO ELEGIDO CON E'XITO!\n";

		if(f==1){

			ruta="OESTE - CAQUETA";

			precio="6";

			distancia="3,2 KM";

			tiempo="10";
		}
		if(f==2){

			ruta="OESTE - RAMO'N CASTILLA";

			precio="10";

			distancia="4,9 KM";

			tiempo="17";
		}
		if(f==3){

			ruta="OESTE - AV. TINGO MARI'A";

			precio="12";

			distancia="5,6 KM";

			tiempo="20";
		}
		if(f==4){

			ruta="OESTE - MERCADO COLONIAL";

			precio="16";

			distancia="8,1 KM";

			tiempo="22";
		}
		if(f==5){

			ruta="OESTE - MEGAPLAZA BELLAVISTA";

			precio="21";

			distancia="11 KM";

			tiempo="27";
		}
		if(f==6){

			ruta="OESTE - AV. SANTA ROSA";

			precio="23";

			distancia="12,6 KM";

			tiempo="33";
		}

		if(f<7 && f>0){
			submenuvehiculoOESTE(puntero2,ruta,precio,distancia,tiempo);}

		if(f==7){
			submenuruta(puntero2);
		}
		else{
			cout << "\n!ERROR!- HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuparaderoOESTE(puntero2);
		}
	}

	void submenuruta(string puntero1){
		int e;
	    cout << "\n   ********************************\n";
		cout << "\n           RUTAS DISPONIBLES        \n";
		cout << "\n             1. NORTE \n";
		cout << "\n             2. SUR \n";
		cout << "\n             3. ESTE \n";
		cout << "\n             4. OESTE \n";
		cout << "\n             5. ATRA'S \n";
		cout << "\n   ********************************\n";
		cout << "\n";
		cin >> e;
system("cls");

		if(e==1){submenuparaderoNORTE(puntero1);}
		if(e==2){submenuparaderoSUR(puntero1);}
		if(e==3){submenuparaderoESTE(puntero1);}
		if(e==4){submenuparaderoOESTE(puntero1);}
		if(e==5){submenuturno();}
		else{
			cout << "\n!ERROR!- HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuruta(puntero1);}
		}
