#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

struct conductor{
	string nombre;
	int edad;
	string placa;
	string referencia;
	string codigo;
}
conductorN_M1_V1={"MAGNUS",37,"UQ 4567","LE GUSTA EL DEPORTE","N0M10V1"},
conductorN_M2_V1={"BASIL",38,"UB 4547","SE ALIMENTA SALUDABLEMENTE","N0M20V1"},
conductorN_M3_V1={"YOPI",40,"AF 4322 ","LE GUSTA LA MUSICA RAP","N0M30V1" },
conductorN_T1_V1={"JEAN PIERRE",56,"TV 2459","PARTICIPO- EN UNA MARATO-N QUEDANDO EN 2DO PUESTO","N0T10V1"},
conductorN_T2_V1={"NESTOR",49, "RM 2341", "LE GUSTA LA MUSICA INDIE","N0T20V1"},
conductorN_T3_V1={"BRIGITTE",41, "ZS 5675", "BAILA MARINERA DESDE LOS 12","N0T30V1"},
conductorN_N1_V1={"DERECK",42,"DF 2345","LE GUSTA LA MUSICA ROCK","N0N10V1"},
conductorN_N2_V1={"MICHELLE",29 ,"GB 2452", "LE GUSTA LA MUSICA INDIE","N0N20V1"},
conductorN_N3_V1={"ANTOINE",30, "YH 9768", "LE GUSTA LA MUSICA COUNTRY","N0N30V1"};

void submenuvehiculoNORTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuparaderoNORTE(string puntero3);
void submenuboleto(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7, string puntero8);

void submenuvehiculoNORTE_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoNORTE_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);


void submenuvehiculoNORTE_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_M1_V1.codigo;

			cout << "\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n       NOMBRE: " << conductorN_M1_V1.nombre <<"\n";
			cout <<"\n       EDAD: " <<conductorN_M1_V1.edad << "\n";
			cout <<"\n       PLACA: " <<conductorN_M1_V1.placa << "\n";
			cout <<"\n       REFERENCIA: " <<conductorN_M1_V1.referencia << "\n";
			cout <<"\n       1. VOLVER A ELEGIR\n" ;
			cout <<"\n       2. ACEPTAR\n";
			cout << "\n   **************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}

void submenuvehiculoNORTE_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_M2_V1.codigo;
	        cout <<"\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n         NOMBRE: " << conductorN_M2_V1.nombre << "\n";
			cout <<"\n         EDAD: " <<conductorN_M2_V1.edad << "\n";
			cout <<"\n         PLACA: " <<conductorN_M2_V1.placa <<"\n" ;
			cout <<"\n         REFERENCIA: " <<conductorN_M2_V1.referencia <<"\n" ;
			cout <<"\n         1. VOLVER A ELEGIR\n" ;
			cout <<"\n         2. ACEPTAR\n" ;
			cout <<"\n   **************************************************";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}

void submenuvehiculoNORTE_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_M3_V1.codigo;
			cout <<"\n   **************************************************\n"<< "\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n      NOMBRE: " << conductorN_M3_V1.nombre << "\n";
			cout <<"\n      EDAD: " <<conductorN_M3_V1.edad << "\n";
			cout <<"\n      PLACA: " <<conductorN_M3_V1.placa << "\n";
			cout <<"\n      REFERENCIA: " <<conductorN_M3_V1.referencia << "\n";
			cout <<"\n      1. VOLVER A ELEGIR\n" ;
			cout <<"\n      2. ACEPTAR\n";
			cout << "\n**************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}

}

void submenuvehiculoNORTE_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_T1_V1.codigo;

			cout <<"\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n      NOMBRE: " << conductorN_T1_V1.nombre << "\n";
			cout <<"\n      EDAD: " <<conductorN_T1_V1.edad << "\n";
			cout <<"\n      PLACA: " <<conductorN_T1_V1.placa << "\n";
			cout <<"\n      REFERENCIA: " <<conductorN_T1_V1.referencia << "\n";
			cout <<"\n      1.VOLVER A ELEGIR\n" ;
			cout <<"\n      2.ACEPTAR\n" ;

			cout << "\n**************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}

}

void submenuvehiculoNORTE_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_T2_V1.codigo;

			cout << "\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n       NOMBRE: " << conductorN_T2_V1.nombre << "\n";
			cout <<"\n       EDAD: " <<conductorN_T2_V1.edad << "\n";
			cout <<"\n       PLACA: " <<conductorN_T2_V1.placa << "\n";
			cout <<"\n       REFERENCIA: " <<conductorN_T2_V1.referencia << "\n";
			cout <<"\n       1.VOLVER A ELEGIR\n";
			cout <<"\n       2.ACEPTAR\n" ;
			cout << "\n   **************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}

}

void submenuvehiculoNORTE_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_T3_V1.codigo;

			cout <<"\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n       NOMBRE: " << conductorN_T3_V1.nombre << "\n";
			cout <<"\n       EDAD: " <<conductorN_T3_V1.edad << "\n";
			cout <<"\n       PLACA: " <<conductorN_T3_V1.placa << "\n";
			cout <<"\n       REFERENCIA: " <<conductorN_T3_V1.referencia <<"\n";
			cout <<"\n       1.VOLVER A ELEGIR\n" ;
			cout <<"\n       2.ACEPTAR\n";
			cout << "\n   **************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}

}

void submenuvehiculoNORTE_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_N1_V1.codigo;

			cout <<"\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n      NOMBRE: " << conductorN_N1_V1.nombre <<"\n";
			cout <<"\n      EDAD: " <<conductorN_N1_V1.edad << "\n";
			cout <<"\n      PLACA: " <<conductorN_N1_V1.placa << "\n";
			cout <<"\n      REFERENCIA: " <<conductorN_N1_V1.referencia << "\n";
			cout <<"\n      1.VOLVER A ELEGIR\n" ;
			cout <<"\n      2.ACEPTAR\n" ;

			cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}

}

void submenuvehiculoNORTE_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_N2_V1.codigo;

			cout <<"\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";

			cout <<"\n      NOMBRE: " << conductorN_N2_V1.nombre << "\n";
			cout <<"\n      EDAD: " <<conductorN_N2_V1.edad << "\n";
			cout <<"\n      PLACA: " <<conductorN_N2_V1.placa << "\n";
			cout <<"\n      REFERENCIA: " <<conductorN_N2_V1.referencia << "\n";
			cout <<"\n      1.VOLVER A ELEGIR\n" ;
			cout <<"\n      2.ACEPTAR\n" ;

			cout << "\n   **************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}

}

void submenuvehiculoNORTE_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorN_N3_V1.codigo;

			cout <<"\n   **************************************************\n";
			cout <<"\n                  DATOS DEL CONDUCTOR                \n";
			cout <<"\n      NOMBRE: " << conductorN_N3_V1.nombre << "\n";
			cout <<"\n      EDAD: " <<conductorN_N3_V1.edad << "\n";
			cout <<"\n      PLACA: " <<conductorN_N3_V1.placa << "\n";
			cout <<"\n      REFERENCIA: " <<conductorN_N3_V1.referencia << "\n";
			cout <<"\n      1.VOLVER A ELEGIR\n" ;
			cout <<"\n      2.ACEPTAR\n" ;
			cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");


			if(h==1){
				submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoNORTE_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}



void submenuvehiculoNORTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){

	string punteroM1;

	punteroM1="HORARIO M1";
	if(puntero3==punteroM1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}


		/*cout<<"nombre: " << conductor1.nombre;
		cout<<"edad: " <<conductor1.edad;
		cout<<"placa: " <<conductor1.placa;
		cout<<"referncia: " <<conductor1.referencia;*/
	}


	string punteroM2;

	punteroM2="HORARIO M2";
	if(puntero3==punteroM2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroM3;

	punteroM3="HORARIO M3";
	if(puntero3==punteroM3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroT1;

	punteroT1="HORARIO T1";
	if(puntero3==punteroT1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroT2;

	punteroT2="HORARIO T2";
	if(puntero3==punteroT2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroT3;

	punteroT3="HORARIO T3";
	if(puntero3==punteroT3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroN1;

	punteroN1="HORARIO N1";
	if(puntero3==punteroN1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroN2;

	punteroN2="HORARIO N2";
	if(puntero3==punteroN2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


	string punteroN3;

	punteroN3="HORARIO N3";
	if(puntero3==punteroN3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){submenuvehiculoNORTE_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){submenuparaderoNORTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoNORTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}
}
