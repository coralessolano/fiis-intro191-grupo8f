#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

struct conductor{
	string nombre;
	int edad;
	string placa;
	string referencia;
	string codigo;
}
conductorE_M1_V1={"FRANK",31, "CH 3333", "CONOCE MUCHOS ATAJOS, EL LLEGA MUY TEMPRANO","E0M10V1"},
conductorE_M2_V1={"FREDDY",43, "OL 3493", "LE GUSTA LA MUSICA POP Y PARTICIPO EN LA VOZ PERO NO PASO EL CASTING","E0M20V1"},
conductorE_M3_V1={"DAVID",35,"TT 0999","ES PEQUENIO PERO MUY VALIENTE, UN DI-A SALVO A UN INFANTE EN UN INCENDIO","E0M30V1"},
conductorE_T1_V1={"ALEJANDRO",37, "AT 7899", "TIENE PASION POR LA REPOSTERIA, HA PARTICIPADO EN MUCHOS CONCURSOS","E0T10V1"},
conductorE_T2_V1={"HETHER",40, "VV 8879","LE GUSTA LA JUSTICIA Y SIEMPRE TIENE CAMBIO PARA TU VUELTO","E0T20V1"},
conductorE_T3_V1={"JESSIE",26, "IL 9044", "DE PEQUENIA APRENDIO TRUCOS DE MAGIA","E0T30V1"},
conductorE_N1_V1={"CURT",32, "OP 3802", "NACIO EN COLOMBIA Y ES MUY CHISTOSO","E0N10V1" },
conductorE_N2_V1={"URSULA",31, "EP 3234" , "NACIO EN VENEZUELA Y LE GUSTA LA SALSA","E0N20V1" },
conductorE_N3_V1={"INES",42, "EX 1234" ,  "JUEGA PING PONG","E0N30V1" };

void submenuvehiculoESTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuparaderoESTE(string puntero3);
void submenuboleto(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7, string puntero8);

void submenuvehiculoESTE_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);
void submenuvehiculoESTE_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7);

void submenuvehiculoESTE(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){

	string punteroM1;

	punteroM1="HORARIO M1";
	if(puntero3==punteroM1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO-N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroM2;

	punteroM2="HORARIO M2";
	if(puntero3==punteroM2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroM3;

	punteroM3="HORARIO M3";
	if(puntero3==punteroM3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}


		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroT1;

	punteroT1="HORARIO T1";
	if(puntero3==punteroT1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroT2;

	punteroT2="HORARIO T2";
	if(puntero3==punteroT2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCION INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroT3;

	punteroT3="HORARIO T3";
	if(puntero3==punteroT3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}



	string punteroN1;

	punteroN1="HORARIO N1";
	if(puntero3==punteroN1){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroN2;

	punteroN2="HORARIO N2";
	if(puntero3==punteroN2){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}

	string punteroN3;

	punteroN3="HORARIO N3";
	if(puntero3==punteroN3){
		int g;
		cout << "\n   ********************************\n";
		cout << "\n         VEHI'CULOS DISPONIBLES \n";
	    cout << "\n          1. VEHI'CULO 1  \n";
		cout << "\n          2. ATRA'S \n";
		cout << "\n   ********************************\n";
		cin >> g;
		system("cls");

		if(g==1){
			submenuvehiculoESTE_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
		}

		if(g==2){
			submenuparaderoESTE(puntero3);
		}
		else{
			cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
			submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
		}
	}


}

void submenuvehiculoESTE_M1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_M1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_M1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_M1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_M1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_M1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_M1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_M2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_M2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_M2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_M2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_M2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_M2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_M2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_M3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_M3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_M3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_M3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_M3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_M3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_M3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_T1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_T1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_T1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_T1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_T1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_T1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_T1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_T2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_T2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_T2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_T2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_T2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_T2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_T2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_T3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_T3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_T3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_T3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_T3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_T3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_T3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_N1_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_N1_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_N1_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_N1_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_N1_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_N1_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_N1_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_N2_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_N2_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_N2_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_N2_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_N2_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_N2_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_N2_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
void submenuvehiculoESTE_N3_V1(string puntero3, string puntero4,string puntero5,string puntero6,string puntero7){
	int h;
	string codigoconductor=conductorE_N3_V1.codigo;

	cout <<"\n   **************************************************\n";
	cout <<"\n                  DATOS DEL CONDUCTOR                \n";
	cout <<"\n      NOMBRE: " << conductorE_N3_V1.nombre << "\n";
	cout <<"\n      EDAD: " <<conductorE_N3_V1.edad << "\n";
	cout <<"\n      PLACA: " <<conductorE_N3_V1.placa << "\n";
	cout <<"\n      REFERENCIA: " <<conductorE_N3_V1.referencia << "\n";
	cout <<"\n      1.VOLVER A ELEGIR\n" ;
	cout <<"\n      2.ACEPTAR\n" ;
	cout <<"\n   **************************************************\n";
			cin >> h;
			system("cls");

			if(h==1){
				submenuvehiculoESTE(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
			if(h==2){
				submenuboleto(puntero3, puntero4,puntero5,puntero6,puntero7,codigoconductor);

			}
			else{
				cout << "\nERROR-HA SELECCIONADO UNA OPCIO'N INVA'LIDA\n";
				submenuvehiculoESTE_N3_V1(puntero3, puntero4,puntero5,puntero6,puntero7);
			}
}
